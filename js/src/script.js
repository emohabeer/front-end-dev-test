/**
 *
 *	Podcast filter module
 *
 */
(function(win,doc,$) {

	/**
	 * start the application
	 * @return {void}
	 */
	var init = function() {
		console.log('init');
	};

	// initialise the module
	init();


})(window, document, jQuery);


/*** obtain podcast rss data via ajax ***/

var podcastURL = "https://imperialcollegepodcast.libsyn.com/rss";

$.ajax(podcastURL, {
	accepts: {
		xml: "application/rss+xml"
  },

  dataType: "xml",

  success: function(xmldata) {
  	//set initial ID number. This will increment to give each podcast listing a unique ID
  	var podID = 0;
  	$(xmldata).find("item").each(function() {
  		//get full xml content for each podcast item listing
  		var podcastListing = $(this);

  		/*** Get Podcast Overview - extract from the podcast description to obtain overview ***/
  		//full podcast description
  		var podcastDescription = $(this).find("description").html();
  		//extract first paragraph of description
  		var startPosition = podcastDescription.indexOf('<p><strong>') + 28;
  		var endPosition = podcastDescription.indexOf('</strong></p>',startPosition);
  		var overviewText = podcastDescription.substring(startPosition, endPosition);


  		/*** Get Podcast Duration - remove day and timestamp from publication date ***/
  		//full publication date
  		var pubDate = $(this).find("pubDate").text();
  		//shorten publiction date
  		var shortPubDate = pubDate.substring(5, 16);


  		/*** Create and populate podcast cards with content including title, image, overview, date and duration ***/
  		var listingContent = `
          <div class="col-lg-4 podcast-listing" id="` + (++podID) + `">
          	<div class="container">
          		<div class="row">
          			<div class="col-xs-12">
          				<h2>
          					<a href="${podcastListing.find("link").text()}" target="_blank" class="podcast-link">${podcastListing.find("title").text()}</a>
          				</h2>
          			</div>
          			<div>
          				<div class="expand-button" role="button">+</div>
          			</div>
          			<div class="col-12">
          				<img data-original="${podcastListing.find("itunes\\:image").attr("href")}" class="podcast-listing-img" alt="podcast thumbnail - ${podcastListing.find("title").text()}">
	          		</div>
          			<div class="col-12 expandable">
          				<div class="col-12">
	          				<p>${overviewText}</p>
	          			</div>
	          			<div class="col-12">
	          				<p><strong>Date:</strong> ${shortPubDate}</p>
	          				<p><strong>Duration</strong>: ${podcastListing.find("itunes\\:duration").text()}</p>
	          			</div>
	          		</div>
          		</div>
          	</div>
          </div>
        `;
        $("#podcast-list").append(listingContent);
    });
  },

  //initialise LazyLoad on images once ajax call is complete
  complete: function () {
    var myLazyLoad = new LazyLoad({threshold:0});
  }

});

$(document).ready(function() {

	/*** search filter ***/
	//filter upon typing into search bar
	$("#podcast-search-field").on("keyup", function() {
		//store search phrase being typed in
		var searchTerm = $(this).val().toLowerCase();
		//filter podcast listings matching the search term
		$("#podcast-list > div").show().filter(function() { //show the podcast listings
			return $(this).find('div').text().toLowerCase().indexOf(searchTerm) === -1; //check if the listing text contains the search term substring. If not found, return true.
			}).hide(); //hide the listings so that only those meeting search term criteria are displayed
		//re-initialise LazyLoad to load any images in view
		var myLazyLoad = new LazyLoad({threshold:0});
	});

	/*** Podcast card expansion ***/
	//expand/collapse podcast card upon click
	$(document).on("click",".expand-button", function(){ //need to use Event Delegation to select and manipulate content loaded by ajax
		$(this).parent().next().next().slideToggle();
		//display correct expand/collapse icon
		var expandIcon = $(this).html();
		if (expandIcon == "+") {
			$(this).html("-");
		} else {
			$(this).html("+");
		}
	});
	//reset expanded podcast cards when using search bar
	$("#podcast-search-field").click(function() {
		//collapse open podcast cards
		$(".expandable").hide();
		//reset expand icon
		$(".expand-button").html("+");
		//re-initialise LazyLoad to load any images in view
		var myLazyLoad = new LazyLoad({threshold:0});
	});

	/*** sort podcast listings ***/
	//sort podcast listings from newest to oldest
	function sortNewToOld() {
		$("#podcast-list .podcast-listing").sort(function(a, b) {
			return parseInt(a.id) - parseInt(b.id);
		}).each(function() {
			var listing = $(this);
			listing.remove();
			$(listing).appendTo("#podcast-list");
			//re-initialise LazyLoad to load any images in view
			var myLazyLoad = new LazyLoad({threshold:0});
		});
	}

	//sort podcast listings from oldest to newest
	function sortOldToNew() {
		$("#podcast-list .podcast-listing").sort(function(a, b) {
			return parseInt(b.id) - parseInt(a.id);
		}).each(function() {
			var listing = $(this);
			listing.remove();
			$(listing).appendTo("#podcast-list");
			//re-initialise LazyLoad to load any images in view
			var myLazyLoad = new LazyLoad({threshold:0});
		});
	}

	//control sorting via buttons
	$("#sort-new-old").on("click", function () {
		sortNewToOld();
	});

	$("#sort-old-new").on("click", function () {
		sortOldToNew();
	});
});