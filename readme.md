# Imperial InterviewTest - Frontend Developer - November 2021
# Created by Eshan Mohabeer


Created an interpretation of a College podcast landing page.

XML data used for this project has been obtained from the live Imperial College podcast rss feed, at time of writing.

Use search bar to filter podcasts. Use any applicable search terms to filter based on the text contained within the title, overview, date or duration of the podcast cards.

Sort listings by Newest-Oldest or Oldest-Newest via on-screen buttons. Works with both filtered or non-filtered results.

### Additional Notes

* New SVG files used for podcast link buttons
* Stitcher and YouTube SVG logo files required additional editing in Adobe Illustrator, hence slightly larger file sizes
* New colour code added to variables.scss file to use for podcast cards (taken from existing podcast page https://www.imperial.ac.uk/be-inspired/social-and-multimedia/podcasts/)
* Additional SASS files created, to add styling as required
* Project utilises Bootstrap grid. This is simply just the minified version of the grid css file, NOT the full Bootstrap template.
* All javascript edits made to /js/src/script.js file.
* As outlined in the /resources/Front-end-developer-test.docx document, the focus of this project is on the main listing page, based on assumption this would be incorporated into a standard College template with the common header, navigation, and footer. For this reason no attempt has been made to recreate the  header/nav/footer as part of the task.
* Lazy Loading enabled for thumbnail images - local version of minified source file has been obtained from https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/7.2.0/lazyload.min.js

### Task running

This is provided by [Grunt](https://gruntjs.com/).

#### Compile

    grunt

#### Compile when files are changed

    grunt watch

### Package management

Development package management is provided by [npm](https://www.npmjs.com/). Compiled vendor code management is provided by [bower](https://bower.io/).

## Questions

For any issues or questions regarding this project, please contact Eshan Mohabeer - eshan@mohabeer.co.uk
